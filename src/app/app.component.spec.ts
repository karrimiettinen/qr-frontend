import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';
import { TimeoutError } from 'rxjs';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        FormsModule,
        QRCodeModule
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'qr-frontend'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('qr-frontend');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('qr-frontend app is running!');
  });

  it('should have predefined qrCode variable as "test"', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance.qrCode).toEqual("test");
  })

  it('should have input element', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('input')).toBeDefined();
  })

  it('should have qrcode element', async (done) => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    setTimeout(() => {
      expect(compiled.querySelector('.qrcode img')).toBeDefined();
      // console.log({qrcode: compiled.querySelector('.qrcode img')});
      done();
    }, 2000)
  })

  it('qr-code width should be bigger than 0', async (done) => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    setTimeout(() => {
      expect(compiled.querySelector('.qrcode img').width).toBeGreaterThan(0);
      done();
    }, 2000);
  })

  /*it('should insert test input automaticly', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('input').value).toContain('test');
  })*/

  /*it('qr-code should have width and height', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    const imageElement = compiled.querySelector('.qrcode img');
    console.log({imageElement});
    expect(imageElement.width).toBeGreaterThan(0);
    expect(imageElement.height).toBeGreaterThan(0);
  })*/
});
