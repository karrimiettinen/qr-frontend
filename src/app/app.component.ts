import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'qr-frontend';
  qrCode: string = "test";

  constructor() { }

  /**
   * Returns sum of two integer
   * @param a first integer
   * @param b second integer
   * @returns sum
   */
  add(a: number,b: number): number {
    return a+b;
  }
}
